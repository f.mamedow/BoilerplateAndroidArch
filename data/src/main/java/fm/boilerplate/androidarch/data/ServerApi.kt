package fm.boilerplate.androidarch.data

import com.google.gson.annotations.SerializedName
import fm.boilerplate.androidarch.data.dto.PhotoDto
import retrofit2.http.GET

interface ServerApi {

    @GET("photos")
    suspend fun getPhotos() : List<PhotoDto>

}