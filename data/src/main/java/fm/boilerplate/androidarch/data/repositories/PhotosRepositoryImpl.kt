package fm.boilerplate.androidarch.data.repositories

import fm.boilerplate.androidarch.data.ServerApi
import fm.boilerplate.androidarch.data.dto.toDomain
import fm.boilerplate.androidarch.domain.models.Photo
import fm.boilerplate.androidarch.domain.repositories.PhotoRepository
import javax.inject.Inject

class PhotosRepositoryImpl @Inject constructor(
    private val serverApi: ServerApi
) : PhotoRepository {

    override suspend fun fetchPhotos() : List<Photo> {
        val response = serverApi.getPhotos()
        return response.map { it.toDomain() }
    }

}