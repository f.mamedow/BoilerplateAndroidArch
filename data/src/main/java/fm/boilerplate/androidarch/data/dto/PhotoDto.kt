package fm.boilerplate.androidarch.data.dto

import com.google.gson.annotations.SerializedName
import fm.boilerplate.androidarch.domain.models.Photo

data class PhotoDto(
    @SerializedName("albumId")
    val albumId: Long,
    @SerializedName("id")
    val id: Long,
    @SerializedName("title")
    val title: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("thumbnailUrl")
    val thumbnailUrl: String,
)

fun PhotoDto.toDomain() = Photo(
    albumId = albumId,
    id = id,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl,
)
