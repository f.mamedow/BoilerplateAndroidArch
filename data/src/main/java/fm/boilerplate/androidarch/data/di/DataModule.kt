package fm.boilerplate.androidarch.data.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ViewModelComponent
import fm.boilerplate.androidarch.data.ServerApi
import fm.boilerplate.androidarch.data.repositories.PhotosRepositoryImpl
import fm.boilerplate.androidarch.domain.Constants
import fm.boilerplate.androidarch.domain.repositories.PhotoRepository
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module(
    includes = [DataModule.Binder::class]
)
@InstallIn(ViewModelComponent::class)
class DataModule {

    @Provides
    fun provideOkHttpClient() : OkHttpClient {
        return OkHttpClient.Builder()
            .build()
    }

    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun provideServerApi(retrofit: Retrofit) : ServerApi {
        return retrofit.create(ServerApi::class.java)
    }

    @Module
    @InstallIn(ViewModelComponent::class)
    interface Binder {
        @Binds
        fun bindPhotosRepository(photoRepository: PhotosRepositoryImpl) : PhotoRepository
    }
}