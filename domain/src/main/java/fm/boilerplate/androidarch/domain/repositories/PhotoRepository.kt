package fm.boilerplate.androidarch.domain.repositories

import fm.boilerplate.androidarch.domain.models.Photo

interface PhotoRepository {

    suspend fun fetchPhotos() : List<Photo>

}