package fm.boilerplate.androidarch.domain.usecase

import fm.boilerplate.androidarch.domain.Resource
import fm.boilerplate.androidarch.domain.models.response.PhotosResponse
import fm.boilerplate.androidarch.domain.repositories.PhotoRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetPhotosUseCase @Inject constructor(
    private val photoRepository: PhotoRepository
) {

    operator fun invoke() : Flow<Resource<PhotosResponse>> = flow {
        emit(Resource.Loading())
        try {
            val photos = photoRepository.fetchPhotos()
            val response = PhotosResponse(photos = photos)
            emit(Resource.Success(response))
        } catch (e: Exception) {
            emit(Resource.Error(e.message))
        }
    }

}