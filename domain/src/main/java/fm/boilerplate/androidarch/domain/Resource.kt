package fm.boilerplate.androidarch.domain

sealed class Resource<out T> {

    class Loading<T>(val data: T? = null) : Resource<T>()

    class Success<T>(val data: T) : Resource<T>()

    class Error<T>(val message: String?) : Resource<T>()

}
