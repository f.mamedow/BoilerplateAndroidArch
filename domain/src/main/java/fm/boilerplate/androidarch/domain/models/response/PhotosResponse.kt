package fm.boilerplate.androidarch.domain.models.response

import fm.boilerplate.androidarch.domain.models.Photo

data class PhotosResponse(
    val photos: List<Photo>,
)
