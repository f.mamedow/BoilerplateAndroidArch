object Versions {

    const val hilt = "2.44"
    const val retrofit = "2.9.0"
    const val navigation = "2.5.3"
    const val picasso = "2.8"

}