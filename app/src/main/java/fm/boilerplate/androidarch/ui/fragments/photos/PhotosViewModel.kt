package fm.boilerplate.androidarch.ui.fragments.photos

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fm.boilerplate.androidarch.domain.Resource
import fm.boilerplate.androidarch.domain.usecase.GetPhotosUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PhotosViewModel @Inject constructor(
    private val getPhotosUseCase: GetPhotosUseCase,
) : ViewModel() {

    private val _uiState = MutableStateFlow(PhotosUiState())
    val uiState = _uiState.asStateFlow()

    fun loadPhotos() = viewModelScope.launch {
        getPhotosUseCase()
            .collectLatest { resource ->
                when (resource) {
                    is Resource.Error -> {
                        _uiState.update {
                            it.copy(isLoading = false, isError = true)
                        }
                    }
                    is Resource.Loading -> {
                        _uiState.update {
                            it.copy(isLoading = true, isError = false)
                        }
                    }
                    is Resource.Success -> {
                        val photos = resource.data.photos
                        _uiState.update {
                            it.copy(isLoading = false, isError = false, photos = photos)
                        }
                    }
                }
            }
    }


}