package fm.boilerplate.androidarch.ui.fragments.photos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fm.boilerplate.androidarch.R
import fm.boilerplate.androidarch.databinding.FragmentPhotosBinding
import fm.boilerplate.androidarch.ui.adapters.PhotosAdapter
import fm.boilerplate.androidarch.ui.fragments.base.BaseFragment

@AndroidEntryPoint
class PhotosFragment : BaseFragment() {

    private var binding: FragmentPhotosBinding? = null

    private val viewModel: PhotosViewModel by viewModels()

    private val adapter : PhotosAdapter by lazy {
        PhotosAdapter(requireContext(), listOf())
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_photos, container, false)
        binding = FragmentPhotosBinding.bind(view)
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
        observerViewModel()

        viewModel.loadPhotos()
    }

    private fun initRecyclerView() {
        binding?.rvPhotos?.adapter = adapter
    }

    private fun observerViewModel() {
        viewModel.uiState.collectUIState { uiState ->
            binding?.progressContainer?.isVisible = uiState.isLoading
            binding?.errorContainer?.isVisible = uiState.isError
            uiState.photos?.let { adapter.updateData(it) }
        }
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }

}