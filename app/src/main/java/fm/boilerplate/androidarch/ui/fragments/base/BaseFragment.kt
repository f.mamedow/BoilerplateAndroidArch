package fm.boilerplate.androidarch.ui.fragments.base

import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

open class BaseFragment : Fragment() {



    protected fun <T> StateFlow<T>.collectUIState(
        lifecycleState: Lifecycle.State = Lifecycle.State.STARTED,
        action: (T) -> Unit
    ) {
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(lifecycleState) {
                this@collectUIState.collect {
                    action(it)
                }
            }
        }
    }

}