package fm.boilerplate.androidarch.ui.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.squareup.picasso.Picasso
import fm.boilerplate.androidarch.R
import fm.boilerplate.androidarch.databinding.ItemPhotoBinding
import fm.boilerplate.androidarch.domain.models.Photo


class PhotosAdapter(
    context: Context,
    private var data: List<Photo>
) : RecyclerView.Adapter<PhotosAdapter.ViewHolder>() {

    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemPhotoBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val photo = data[position]
        holder.bind(photo)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateData(photos: List<Photo>) {
        data = photos
        notifyDataSetChanged()
    }

    override fun getItemCount() = data.size

    class ViewHolder(private val binding: ItemPhotoBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(photo: Photo) {
            binding.tvName.text = photo.title

            Picasso.get()
                .load(photo.url)
                .error(R.drawable.image_error)
                .placeholder(R.drawable.image_placeholder)
                .into(binding.ivPhoto)
        }

    }

}