package fm.boilerplate.androidarch.ui.fragments.photos

import fm.boilerplate.androidarch.domain.models.Photo

data class PhotosUiState(
    val isLoading: Boolean = false,
    val isError: Boolean = false,
    val photos: List<Photo>? = null
)
